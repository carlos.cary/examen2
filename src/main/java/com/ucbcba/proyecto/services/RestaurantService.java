package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Restaurant;

public interface RestaurantService {

    Iterable<Restaurant> listAllRestaurants();

    Restaurant getRestaurantById(Integer id);

    Restaurant saveRestaurant(Restaurant comment);

    void deleteRestaurant(Integer id);

}

