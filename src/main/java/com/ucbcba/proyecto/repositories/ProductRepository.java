package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Product;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface ProductRepository extends CrudRepository<Product,Integer> {
}
